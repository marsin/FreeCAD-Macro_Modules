BINDIR    = ~/.FreeCAD/Macro
MODULEDIR = mars_modules
# MODULE1   =
# MACRO1    =

INSTALL = install --mode=644
LINK    = ln -s -r -f
RM      = rm -f
CPDIR   = cp -r
# MKDIR   = mkdir -p
RMDIR   = rm -rf


usage:
	@echo "usage:"
	@echo "  'make link'      links   the macro module directory into the users FreeCAD macro directory"
	@echo "  'make install'   copies  the macro module directory into the users FreeCAD macro directory"
	@echo "  'make uninstall' removes the macro module directory from the users FreeCAD macro directory"

link:
	$(LINK) $(MODULEDIR) $(BINDIR)/$(MODULEDIR)

install:
	$(CPDIR) $(MODULEDIR) $(BINDIR)/$(MODULEDIR)

uninstall:
	$(RMDIR) $(BINDIR)/$(MODULEDIR)

