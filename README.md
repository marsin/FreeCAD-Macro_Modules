FreeCAD Macro Modules
=====================

 Python modules for FreeCAD macros.
 This library was created for my own purposes and comes without any warranty.


Modules
-------

 - `read_csv.py`:     Reads a CSV file into a python list
 - `fill_sheet.py`:   Fills a FreeCAD spreadsheet from a tagged python list
 - `create_sheet.py`: Creates new FreeCAD groups and spreadsheets
 - `get_object.py`:   Get FreeCAD objects from source file

 - `proof.py`:        Proofs parameters for their validity and prints messages
 - `messages.py`:     Prints messages to the FreeCAD Report View console


Installation
------------

 - `make link`:      links   the macro module directory into the users FreeCAD macro directory
 - `make install`:   copies  the macro module directory into the users FreeCAD macro directory
 - `make uninstall`: removes the macro module directory from the users FreeCAD macro directory


Usage
-----

### Examples ###

```python
from mars_modules.read_csv import read_csv_into_data_list
from mars_modules.fill_sheet import fill_sheet_from_data_list
from mars_modules.create_sheet import create_sheet
from mars_modules.create_sheet import create_group
from mars_modules.get_object import get_shape_of_body_by_label as get_body
from mars_modules.get_object import get_sheet_by_label as get_sheet
```


Tagged Python List
------------------

 The tagged python list is used to create FreeCAD spreadsheets
 for parametric constructions from a simple python list.
 The matrix system is limited to maximum 8 columns per row.

 It is more handy to use python lists than CSV files for very long formulas.


### Tags ###

 - `SN`: Spreadsheet Name - Defines the name of the spreadsheet
 - `H1`: Header 1         - Spreadsheet Topic
 - `H2`: Header 2         - Section Topic
 - `HL`: Header Line      - Column Topic
 - `PA`: Paragraph        - A short description about the sheet
 - `BR`: Break            - A empty line
 - `LI`: List Row         - A data line with list values: Name, Value, Coordinate, Annotation, Unit
 - `MX`: Matrix Row       - A data line with matrix values (max. 8 columns)


