#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2021 Martin Singer <martin.singer@web.de>
#
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
@package Fills a FreeCAD spreadsheet from a tagged python list.

A Tagged Python List is required to fill the spreasheet with data.
"""

__title__   = "Fill Sheet"
__about__   = "This module fills a FreeCAD sheet from a tagged Python list"
__url__     = "https://gitlab.com/marsin/FreeCAD-Macro_Modules"
__license__ = "LGPL2.1"
__author__  = "Martin Singer"
__email__   = "martin.singer@web.de"


__LineNr = 1
__ContainsMatrix = False

def fill_sheet_from_data_list(sheet, data):
    global __LineNr
    global __ContainsMatrix

    __LineNr = 1

    # Fill Records
    for record in data:
        if record[0] == "SN":   # Spreadsheet Name
            pass
        elif record[0] == "BR": # Break
            __new_line()
        elif record[0] == "H1": # Header 1
            __enter_line(sheet, record[1])
            __format_line(sheet, 'bold')
            __format_line(sheet, 'underline')
            __new_line()
        elif record[0] == "H2": # Header 2
            __enter_line(sheet, record[1])
            __format_line(sheet, 'bold')
            __new_line()
        elif record[0] == "PA": # Paragraph
            __enter_line(sheet, record[1])
            __format_line(sheet, 'italic')
            __new_line()
        elif record[0] == "HL": # Header Line
            __enter_record(sheet, record[1], record[2], record[3], record[4])
            __format_line(sheet, 'bold')
            __new_line()
        elif record[0] == "LI": # List row
            __enter_record(sheet, record[1], record[2], record[3], record[4])
            __set_alias(sheet, record[1], record[5])
            __format_result(sheet, record[2])
            __new_line()
        elif record[0] == "MX": # Matrix row
            __ContainsMatrix = True
            __enter_matrix(sheet, record[1], record[2], record[3], record[4], record[5], record[6], record[7], record[8], record[9], record[10], record[11])
            __new_line()
        else:
            print("WARNING: unknown tag '{}'".format(record[0]))

    # Format Columns
    sheet.setColumnWidth('A', 25)

    if __ContainsMatrix != True:
        sheet.setColumnWidth('B', 175)
        sheet.setColumnWidth('E', 400)


def __new_line():
    global __LineNr
    __LineNr += 1


def __format_line(sheet, style):
    global __LineNr

    col_first = 'A'  + str(__LineNr)
    col_last  = 'ZZ' + str(__LineNr)
    col_span  = col_first + ':' + col_last

    sheet.setStyle(col_span, style, 'add')


def __enter_line(sheet, text):
    global __LineNr

    col_first = 'A'  + str(__LineNr)
    col_last  = 'ZZ' + str(__LineNr)
    col_span  = col_first + ':' + col_last

    sheet.mergeCells(col_span)
    sheet.set(col_first, text)


def __enter_record(sheet, name, value, coordinates, annotation):
    global __LineNr

    col_b = 'B' + str(__LineNr)
    col_c = 'C' + str(__LineNr)
    col_d = 'D' + str(__LineNr)
    col_e = 'E' + str(__LineNr)

    sheet.set(col_b, name)
    sheet.set(col_c, value)
    sheet.set(col_d, coordinates)
    sheet.set(col_e, annotation)


def __set_alias(sheet, name, unit):
    global __LineNr

    col_c = 'C' + str(__LineNr)
    col_d = 'D' + str(__LineNr)
    col_e = 'E' + str(__LineNr)

    sheet.setDisplayUnit(col_c, unit)
    sheet.setAlias(col_c, name)
    sheet.setStyle(col_d, 'italic', 'add')
    sheet.setStyle(col_e, 'italic', 'add')


def __format_result(sheet, value):
    global __LineNr

    if '=' in value:
        col_c = 'C' + str(__LineNr)
        sheet.setStyle(col_c, 'underline', 'add')


def __enter_matrix(sheet, name, unit, mx_row, mx_col0, mx_col1, mx_col2, mx_col3, mx_col4, mx_col5, mx_col6, mx_col7):
    global __LineNr

    col_b = 'B' + str(__LineNr)
    col_c = 'C' + str(__LineNr)
    col_d = 'D' + str(__LineNr)
    col_e = 'E' + str(__LineNr)
    col_f = 'F' + str(__LineNr)
    col_g = 'G' + str(__LineNr)
    col_h = 'H' + str(__LineNr)
    col_i = 'I' + str(__LineNr)

    sheet.set(col_b, mx_col0)
    sheet.set(col_c, mx_col1)
    sheet.set(col_d, mx_col2)
    sheet.set(col_e, mx_col3)
    sheet.set(col_f, mx_col4)
    sheet.set(col_g, mx_col5)
    sheet.set(col_h, mx_col6)
    sheet.set(col_i, mx_col7)

    if unit != "":
        sheet.setDisplayUnit(col_b, unit)
        sheet.setDisplayUnit(col_c, unit)
        sheet.setDisplayUnit(col_d, unit)
        sheet.setDisplayUnit(col_e, unit)
        sheet.setDisplayUnit(col_f, unit)
        sheet.setDisplayUnit(col_g, unit)
        sheet.setDisplayUnit(col_h, unit)
        sheet.setDisplayUnit(col_i, unit)

    sheet.setAlias(col_b, name + '_R' + str(mx_row) + 'C0')
    sheet.setAlias(col_c, name + '_R' + str(mx_row) + 'C1')
    sheet.setAlias(col_d, name + '_R' + str(mx_row) + 'C2')
    sheet.setAlias(col_e, name + '_R' + str(mx_row) + 'C3')
    sheet.setAlias(col_f, name + '_R' + str(mx_row) + 'C4')
    sheet.setAlias(col_g, name + '_R' + str(mx_row) + 'C5')
    sheet.setAlias(col_h, name + '_R' + str(mx_row) + 'C6')
    sheet.setAlias(col_i, name + '_R' + str(mx_row) + 'C7')
