#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2019 - 2021 Martin Singer <martin.singer@web.de>
# 
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
@package Proofs Parameters.

This module is for similar and messages.

The examination is very general.
It is only tested for typical characteristics.
Implementing better checks is possible if required.
"""

__title__   = "Proof Parameters"
__about__   = "Proofs parameters for their validity and prints messages"
__url__     = "https://gitlab.com/marsin/FreeCAD-Macro_Modules"
__license__ = "LGPL2.1"
__author__  = "Martin Singer"
__email__   = "martin.singer@web.de"


import inspect
from mars_modules.messages import message_caller


def proof_doc(doc):
    """ Proofs that FreeCAD object is not None. """
    if doc is None:
        msg_text = "No document object passed!"
        message_caller(inspect.stack()[1][1], inspect.stack()[1][3], msg_text, "ERROR")
        return False
    return True


def proof_data_list(data_list):
    """ Proofs that tagged list is not None or empty. """
    if data_list is None or data_list == []:
        msg_text = "No data list object passed!"
        message_caller(inspect.stack()[1][1], inspect.stack()[1][3], msg_text, "ERROR")
        return False
    return True


def proof_label(label_string):
    """ Proofs that label string is not None or empty. """
    if label_string is None or label_string == "":
        msg_text = "No label string object passed!"
        message_caller(inspect.stack()[1][1], inspect.stack()[1][3], msg_text, "ERROR")
        return False
    return True


def proof_obj_list(obj_list, obj_label):
    """
    """
    if obj_list is None or obj_list == []:
        msg_text = "No objects with label '{}' in list!".format(obj_label)
        message_caller(inspect.stack()[1][1], inspect.stack()[1][3], msg_text, "ERROR")
        return False
    return True

