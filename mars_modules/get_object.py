#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2019 - 2021 Martin Singer <martin.singer@web.de>
# 
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
@package Get FreeCAD objects from source file.
"""


__title__   = "Get Object"
__about__   = "Get FreeCAD objects from source file"
__url__     = "https://gitlab.com/marsin/FreeCAD-Macro_Modules"
__license__ = "LGPL2.1"
__author__  = "Martin Singer"
__email__   = "martin.singer@web.de"


from mars_modules.proof import proof_obj_list


def get_object_by_label(source_doc, object_label):
    """ Get the FreeCAD object from a source document by its label string.

    @param[in] source_doc    The FreeCAD source document of the requested object
    @param[in] object_label  The label of the requested FreeCAD object in the source document
    @retval    None          Requested object was not found
    @return                  Requested object
    """
    obj_list = source_doc.getObjectsByLabel(object_label)
    if proof_obj_list(obj_list, object_label) != True:
        return None
    return obj_list[0]


def get_object_shape_by_label(source_doc, object_label):
    """ Get the shape of a FreeCAD object from a source document by its label string.

    @param[in] source_doc    The FreeCAD source document of the requested object
    @param[in] object_label  The label of the requested FreeCAD object in the source document
    @retval    None          Requested object was not found
    @return                  Shape of requested object
    """
    obj = get_object_by_label(source_doc, object_label)
    if obj is None:
        return None
    return obj.Shape.removeSplitter()  # Refine shape of object


# TODO: remove deprecated function (exists just for compatibility)
def get_shape_of_body_by_label(source_doc, body_label):
    print("'get_shape_of_body_by_label()' is deprecated, use 'get_object_shape_by_label()' instead!")
    return get_object_shape_by_label(source_doc, body_label)


# TODO: remove deprecated function (exists just for compatibility)
def get_sheet_by_label(source_doc, sheet_label):
    print("'get_sheet_by_label()' is deprecated, use 'get_object_by_label()' instead!")
    return get_object_by_label(source_doc, sheet_label)
