#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2021 Martin Singer <martin.singer@web.de>
# 
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
Read Comma Separated Value file
===============================

## CSV ##

  - Character Set: UTF-8
  - Field delimeter:  `,`
  - String delimeter: `"`
"""


import csv

__title__   = "Read Comma Separated Value file"
__about__   = "Reads CSV file into python list"
__url__     = "https://gitlab.com/marsin/FreeCAD-Macro_Modules"
__license__ = "LGPL2.1"
__author__  = "Martin Singer"
__email__   = "martin.singer@web.de"


def read_csv_into_data_list(csv_file_name):
    data_list = []
    with open(csv_file_name) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')

        for row in csv_reader:
            data_list.append(row)

    return data_list
