#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2019 - 2021 Martin Singer <martin.singer@web.de>
# 
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
@package Creates new FreeCAD groups and spreadsheets.


Documentation
-------------

 - [FreeCAD Wiki: Object name](https://wiki.freecadweb.org/Object_name)
"""

__title__   = "Create Sheet"
__about__   = "This module creates a FreeCAD Sheet tagged Python list"
__url__     = "https://gitlab.com/marsin/FreeCAD-Macro_Modules"
__license__ = "LGPL2.1"
__author__  = "Martin Singer"
__email__   = "martin.singer@web.de"


import re  # regex
import inspect

import FreeCAD
from FreeCAD import Console

from mars_modules.fill_sheet import fill_sheet_from_data_list
from mars_modules.messages import message
from mars_modules.proof import proof_doc
from mars_modules.proof import proof_label
from mars_modules.proof import proof_data_list


__STR_SPREADSHEETS = "Spreadsheets"
__STR_SPREADSHEET = "Spreadsheet"
__STR_SN = "SN"                     # SN: "spreadsheet name"


def autocreate_and_fill_sheet(doc, data_list, replace=True):
    """ Creates a FreeCAD spreadsheet and fills it from the Tagged Python List.

    This function creates a FreeCAD spreadsheet and fills it with the data from
    Tagged Python List.
    It uses the first List Topic (`H1`) line from the list for the spreadsheet label.
    The spreadsheet will be created in the group __STR_SPREADSHEETS.
    If the group does not already exist, it will be created.

    @param[in] doc        FreeCAD document object to create the list in
    @param[in] data_list  Tagged python list with the spreadsheet values
    @param[in] replace    Replace or keep a sheet if it already exists (creates a second sheet)
    """

    if proof_doc(doc) != True:
        return None

    if proof_data_list(data_list) != True:
        return None

    sheet_label = ""

    for record in data_list:
        if record[0] == __STR_SN:
            sheet_label = record[1]
            sheet_label = sheet_label.strip()            # remove leading and appending spaces
            sheet_label = re.sub(" ", "_", sheet_label)  # substitute separator spaces with underlines
            break
    else:
        message("Found no spreadsheet label in tagged list!", "ERROR")
        # Console.PrintError("\n"
        #         "ERROR in '{}' '{}':"
        #         "Found no spreadsheet label in tagged list!".format(
        #             __name__, inspect.stack()[0][3]))  # name of this function
        return None

    return create_and_fill_sheet(doc, __STR_SPREADSHEETS, sheet_label, data_list, replace)


def create_and_fill_sheet(doc, group_label, sheet_label, data_list, replace=True):
    """ Creates a FreeCAD spreadsheet and fills it from the Tagged Python List.

    This function creates a FreeCAD spreadsheet and fills it with the data from
    Tagged Python List. If no valid spreadsheet label string is passed to the function,
    it uses the string __STR_SPREADSHEET for the spreadsheet label.
    If no group is given to the function, a group labeled __STR_SPREADSHEETS will be created.

    @param[in] doc          FreeCAD document object to create the list in
    @param[in] group_label  String with the label of the group to move the new sheet in
    @param[in] sheet_label  String with the label of the new spreadsheet
    @param[in] data_list    Tagged python list with the spreadsheet values
    @param[in] replace      Replace or keep a sheet if it already exists (creates a second sheet)
    @return                 Created FreeCAD spreadsheet
    @retval    None         Spreadsheet was not created
    """

    if proof_doc(doc) != True:
        return None

    if proof_data_list(data_list) != True:
        return None

    sheet = None
    group = None

    if proof_label(group_label) == True:
        group = __get_group(doc, group_label)

    if proof_label(sheet_label) != True:
        sheet_label = __STR_SPREADSHEET

    sheet = create_sheet(doc, sheet_label, replace)
    if sheet is not None:
        fill_sheet_from_data_list(sheet, data_list)

        if group is not None:
            group.addObject(sheet)

    doc.recompute()
    return sheet


def create_sheet(doc, sheet_label, replace=True):
    """ Add a new, or refill the first existing FreeCAD spreadsheet with this label.

    @param[in] doc          FreeCAD document object
    @param[in] sheet_label  String with the label of the new sheet
    @param[in] replace      Replace or keep a sheet if it already exists (creates a second sheet)
    @return                 The created FreeCAD spreadsheet object
    @retval    None         No sheet was created
    """

    sheet = None

    if replace == True:
        # __del_sheets(doc, sheet_label)
        # sheet = __add_sheet(doc, sheet_label)
        __clear_sheets(doc, sheet_label)
        sheet = __get_sheet(doc, sheet_label)
    else:
        sheet = __get_sheet(doc, sheet_label)

    doc.recompute()
    return sheet


def __get_sheet(doc, sheet_label):
    """ Get the first FreeCAD spreadsheet object with the passed label string.

    If a spreadsheet with the passed label string exists, this spreadsheet will be returned.
    If no spreadsheet with the passed label was found, a new spreadsheet will be created.

    @param[in] doc          FreeCAD document object
    @param[in] sheet_label  String with the label of the new sheet
    @return                 The requested FreeCAD spreadsheet object
    @retval    None         No spreadsheet was found or created
    """

    sheet = None

    sheet_list = doc.getObjectsByLabel(sheet_label)
    if sheet_list == []:
        sheet = __add_sheet(doc, sheet_label)
    else:
        sheet = sheet_list[0]
        message("Found a existing FreeCAD spreadsheet with the label '{}'".format(
            sheet.Label), "MESSAGE")
        # Console.PrintMessage("\n"
        #         "MESSAGE from '{}' '{}': "
        #         "Found a existing FreeCAD spreadsheet with the label '{}'".format(
        #             __name__, inspect.stack()[0][3], sheet.Label))

    return sheet


def __add_sheet(doc, sheet_label):
    """ Add a new FreeCAD spreadsheet.

    Adds always a new FreeCAD spreadsheet object.

    @param[in] doc          FreeCAD document object
    @param[in] sheet_label  String with the label of the new sheet
    @return                 The created FreeCAD spreadsheet object
    @retval    None         No sheet was created
    """

    sheet = doc.addObject("Spreadsheet::Sheet", sheet_label)
    if sheet is None:
        message("Creating a new FreeCAD spreadsheet with the label '{}' failed !".format(
            sheet_label), "ERROR")
        # Console.PrintError("\n"
        #         "ERROR in '{}' '{}': "
        #         "Creating a new FreeCAD spreadsheet with the label '{}' failed !".format(
        #             __name__, inspect.stack()[0][3], sheet_label))
    else:
        message("Created new FreeCAD spreadsheet with the label '{}'".format(
            sheet.Label), "MESSAGE")
        # Console.PrintMessage("\n"
        #         "MESSAGE from '{}' '{}': "
        #         "Created new FreeCAD spreadsheet with the label '{}'".format(
        #             __name__, inspect.stack()[0][3], sheet.Label))
    return sheet


def __del_sheets(doc, sheet_label):
    """ Delete all FreeCAD spreadsheets with the passed label string.

    @param[in] doc          FreeCAD document object
    @param[in] sheet_label  String with the label of the new sheet
    """

    sheet_list = doc.getObjectsByLabel(sheet_label)
    for sheet in sheet_list:
        message("Removing existing spreadsheet with label '{}'".format(
            sheet.Label), "MESSAGE")
        # Console.PrintMessage("\n"
        #         "MESSAGE from '{}' '{}': "
        #         "Removing existing spreadsheet with label '{}'".format(
        #             __name__, inspect.stack()[0][3], sheet.Label))
        doc.removeObject(sheet.Name)


def __clear_sheets(doc, sheet_label):
    """ Clears all FreeCAD spreadsheets with the passed label string.

    @param[in] doc          FreeCAD document object
    @param[in] sheet_label  String with the label of the new sheet
    """

    sheet_list = doc.getObjectsByLabel(sheet_label)
    for sheet in sheet_list:
        message("Clearing existing spreadsheet with label '{}'".format(
            sheet.Label), "MESSAGE")
        sheet.clearAll()



def create_group(doc, group_label, enforced=False):
    """ Creates a FreeCAD group.

    Creates a FreeCAD group with the passed group label.
    It is possible to enforce an additional group with this label,
    when a group already exists in the document.

    @param[in] doc          FreeCAD document object to create the group in
    @param[in] group_label  String for the new FreeCAD group label
    @param[in] enforce      Create an additional group, even if a group with this label already exists (optional)
    @return                 FreeCAD group object with passed label (new or already existing)
    @retval    None         No group with the passed label was found or could be created
    """

    if proof_doc(doc) != True:
        return None

    if proof_label(group_label) != True:
        return None

    group = None

    if enforced == True:
        group = __add_group(doc, group_label)
    else:
        group = __get_group(doc, group_label)

    return group


def __get_group(doc, group_label):
    """ Get a FreeCAD group object with the passed label string.

    If a group with the passed label string exists, this group will be returned.
    If no group with the passed label was found, a new group will be created.

    @param[in] doc          FreeCAD document object
    @param[in] group_label  String with the label of the new group
    @return                 The requested FreeCAD group object
    @retval    None         No group was found or created
    """

    group = None

    group_list = doc.getObjectsByLabel(group_label)
    if group_list == []:
        group = __add_group(doc, group_label)
    else:
        group = group_list[0]
        message("Found a existing FreeCAD group with the label '{}'".format(
            group.Label), "MESSAGE")
        # Console.PrintMessage("\n"
        #         "MESSAGE from '{}' '{}': "
        #         "Found a existing FreeCAD group with the label '{}'".format(
        #             __name__, inspect.stack()[0][3], group.Label))

    return group



def __add_group(doc, group_label):
    """ Add a new FreeCAD group object.

    Adds always a new FreeCAD group object.

    @param[in] doc          FreeCAD document object to create the group in
    @param[in] group_label  String for the new FreeCAD group label
    @return                 New FreeCAD group object
    @retval    None         Group was not created
    """

    group = doc.addObject("App::DocumentObjectGroup", group_label)
    if group is None:
        message("Creating new FreeCAD group '{}' failed!".format(
            group_label), "ERROR")
        # Console.PrintError("\n"
        #         "ERROR in '{}' '{}': "
        #         "Creating new FreeCAD group '{}' failed!".format(
        #             __name__, inspect.stack()[0][3], group_label))
    else:
        message("Created new FreeCAD group '{}'".format(
            group.Label), "MESSAGE")
        # Console.PrintMessage("\n"
        #         "MESSAGE from '{}' '{}': "
        #         "Created new FreeCAD group '{}'".format(
        #             __name__, inspect.stack()[0][3], group.Label))
    return group


# def __proof_doc(doc):
#     """ Proofs that FreeCAD object is not None. """

#     if doc is None:
#         Console.PrintError("\n"
#                 "ERROR in '{}' '{}': "
#                 "No document object passed!".format(
#                     __name__, inspect.stack()[1][3]))  # name of the calling function
#         return False
#     return True


# def __proof_data_list(data_list):
#     """ Proofs that tagged list is not None or empty. """

#     if data_list is None or data_list == []:
#         Console.PrintError("\n"
#                 "ERROR in '{}' '{}': "
#                 "No data list passed!".format(
#                     __name__, inspect.stack()[1][3]))
#         return False
#     return True


# def __proof_label(label_string):
#     """ Proofs that label string is not None or empty. """

#     if label_string is None or label_string == "":
#         Console.PrintError("\n"
#                 "ERROR in '{}' '{}':"
#                 "No label string passed!".format(
#                     __name__, inspect.stack()[1][3]))
#         return False
#     return True
