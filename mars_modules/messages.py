#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2019 - 2021 Martin Singer <martin.singer@web.de>
# 
# This macro is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This macro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

"""
@package Prints messages to the FreeCAD Report View console.


## References

 - <https://docs.python.org/3/library/inspect.html#inspect.stack>

 - <https://wiki.freecadweb.org/Report_view>
 - <https://wiki.freecadweb.org/Console_API>
"""

from FreeCAD import Console
import inspect


def message(msg_text, msg_type="MESSAGE"):
    """ Prints a message with calling module and function name.

    This function adds the passed message type string
    and the calling module and function name from the python stack.

    @see message_caller()
    @see message_raw()

    @param[in] msg_text  String with the message to show, or to log
    @param[in] msg_type  String with the message type (optional)
    """
    if msg_text is not None or msg_text != "":
        # caller_filename = inspect.stack()[1][1].split(os.sep)[-1]
        caller_filename = inspect.stack()[1][1]
        caller_function = inspect.stack()[1][3]

        message_caller(caller_filename, caller_function, msg_text, msg_type)


def message_caller(caller_filename, caller_function, msg_text, msg_type="MESSAGE"):
    """ Prints a message with all the passed parameters.

    This function connects the various strings like message text, message type
    as well as the calling function and module name.

    @see message_raw()

    @param[in] caller_filename  String with the name of the calling file
    @param[in] caller_function  String with the name of the calling function
    @param[in] msg_text         String with the message to show, or to log
    @param[in] msg_type         String with the message type (optional)
    """
    msg_string = "\n{} from module '{}' function '{}': {}".format(
        msg_type, caller_filename, caller_function, msg_text)

    message_raw(msg_string, msg_type)


def message_raw(msg_string, msg_type="MESSAGE"):
    """ Prints the passed message string on the FreeCAD Report View or Log.

    @param[in] msg_string  String with the string to show, or to log
    @param[in] msg_type    String with the message type (optional)


    ## Message Types

     - `ERROR`
     - `WARNING`
     - `LOG`
     - `MESSAGE`
     - `None` or empty string, is like MESSAGE
    """
    if msg_string is not None or msg_string != "":
        if msg_type == "ERROR":
            Console.PrintError(msg_string)
        elif msg_type == "WARNING":
            Console.PrintWarning(msg_string)
        elif msg_type == "LOG":
            Console.PrintLog(msg_string)
        else:
            Console.PrintMessage(msg_string)

